import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { DirectionService } from '../../services/directionService';
import { StatusBar } from '@ionic-native/status-bar';

/**
 * Generated class for the BusquedaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-busqueda',
  templateUrl: 'busqueda.html',
})
export class BusquedaPage {

  place: string;
  places: any[];

  constructor(public navCtrl: NavController, public navParams: NavParams, public directionService: DirectionService,
    private zone: NgZone, public events: Events, private statusBar : StatusBar
  ) {
    this.places = [];
  }

  search() {
    this.directionService.findPlaces(this.place)
      .map(x => {
        return x.map(element => {
          return {
            formatted_address: element.formatted_address,
            location: element.geometry.location
          }
        })
      })
      .subscribe(s => {
        this.zone.run(()=>{
          this.places = s;
        });
      });
  }

  setPlace(place: any) {
   this.events.publish("setPlace", place);
    this.navCtrl.pop();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BusquedaPage');
  }
  ionViewDidEnter() {
    this.statusBar.show();
  }

}
