import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { LoginService } from '../../services/loginService';
import { MapaPage } from '../mapa/mapa';
import { SesionService } from '../../services/sesionService';
import { StorageService } from '../../services/storageService';
import { MyvehiculesPage } from '../myvehicules/myvehicules';
import { StatusBar } from '@ionic-native/status-bar';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private LoginService: LoginService, private platform: Platform, private session: SesionService,
    private storageService: StorageService, private statusBar: StatusBar
  ) {
    this.platform.ready().then(() => {
      this.statusBar.backgroundColorByHexString("488aff");
    });
  }

  login() {
    this.LoginService.login()
      .map(x => { return { "access_token": x }; })
      .subscribe(a => {
        this.session.setCredentials(a);
        this.navCtrl.setRoot(MapaPage);
      });
    /*
  this.LoginService.loginBrowser()
    .map(x=> {return JSON.parse(x);})
    .subscribe(a => {
      this.session.setCredentials(a);
      this.navCtrl.setRoot(MapaPage);
    });
*/
  }
  ionViewDidEnter() {
    this.statusBar.hide();
  }

}
