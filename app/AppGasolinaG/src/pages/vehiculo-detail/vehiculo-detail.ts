import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { StorageService } from '../../services/storageService';

/**
 * Generated class for the VehiculoDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-vehiculo-detail',
  templateUrl: 'vehiculo-detail.html',
})
export class VehiculoDetailPage {

    vehicule: any;
    isConsumoPersonalizado: boolean;
    consumoPersonalizado: number;
    isReadOnly : boolean;

  constructor(public navCtrl: NavController, public navParams: NavParams, private storageService: StorageService,
      public alertCtrl: AlertController
  ) {
      this.vehicule= {};
      this.isReadOnly = navParams.get("readonly");
      this.vehicule = navParams.get("current");
      this.consumoPersonalizado = navParams.get("consumoPersonalizado");
      this.isConsumoPersonalizado= this.consumoPersonalizado!==null;
  }

  save(){
    let arreglo= [];
    arreglo.push({"vehiculo": this.vehicule, "consumoPersonalizado": this.consumoPersonalizado });
    this.storageService.save(arreglo)
    .retry(3)
    .subscribe(s=>console.log, s=>console.log,()=>{
      let alert= this.alertCtrl.create({
        title: 'Vehiculos',
        subTitle: 'Se ha guardado correctamente',
        buttons: ['OK']
      });
      alert.present().then(()=>{
          this.navCtrl.pop();
      });  
    });
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad VehiculoDetailPage');
  }

}
