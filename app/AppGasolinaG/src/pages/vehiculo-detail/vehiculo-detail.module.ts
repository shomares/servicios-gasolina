import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VehiculoDetailPage } from './vehiculo-detail';

@NgModule({
  declarations: [
    VehiculoDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(VehiculoDetailPage),
  ],
})
export class VehiculoDetailPageModule {}
