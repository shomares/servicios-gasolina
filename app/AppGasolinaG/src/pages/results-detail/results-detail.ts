import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Alert, AlertController } from 'ionic-angular';
import { StorageService } from '../../services/storageService';

/**
 * Generated class for the ResultsDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-results-detail',
  templateUrl: 'results-detail.html',
})
export class ResultsDetailPage {

  currentVehicule: any;
  currentRoute: any;
  calculo: any;
  vehicule: any;
  constructor(public navCtrl: NavController,
    public navParams: NavParams, 
    public storageService: StorageService,
    public alertCtrl: AlertController) {
    this.currentVehicule = navParams.get("currentVehicule");
    this.currentRoute = navParams.get("currentRoute");
    this.vehicule = this.currentVehicule.vehiculo;
    let kilometros = this.calcularKilometros();

    this.calculo = {
      consumoEco: (kilometros / this.vehicule.rendimientoPorLitro).toFixed(2),
      consumoPersonalizado: this.currentVehicule.consumoPersonalizado!=null?(kilometros / this.currentVehicule.consumoPersonalizado).toFixed(2): "No disponible",
      consumoPromedio: this.currentVehicule.consumoPromedio!=0? (kilometros / this.currentVehicule.consumoPromedio).toFixed(2): "No disponible",
      distancia: kilometros,
      route: this.currentRoute,
      register: this.currentVehicule
    };
  }
  calcularKilometros(): number {
    let salida = 0.0;
    this.currentRoute.legs.forEach(element => {
      salida += element.distance.value;
    });
    return salida/1000;
  }
  add(){
    this.storageService.saveRoute(this.calculo)
    .retry(2)
    .subscribe(s=>{
      let alert= this.alertCtrl.create({
        title: 'Vehiculos',
        subTitle: 'Se ha guardado correctamente',
        buttons: ['OK']
      });
      alert.present().then(()=>{
          this.navCtrl.pop();
      });  

    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ResultsDetailPage');
  }

}
