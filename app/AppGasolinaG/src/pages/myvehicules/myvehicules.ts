import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Navbar } from 'ionic-angular';
import { StorageService } from '../../services/storageService';
import { VehiculoPage } from '../vehiculo/vehiculo';
import { VehiculoDetailPage } from '../vehiculo-detail/vehiculo-detail';
import { StatusBar } from '@ionic-native/status-bar';


/**
 * Generated class for the MyvehiculesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-myvehicules',
  templateUrl: 'myvehicules.html',
})
export class MyvehiculesPage {
  vehicules: any[];
  currentVehicules: any[];
  avance: number;

  constructor(public navCtrl: NavController, public navParams: NavParams, private storage: StorageService,  private statusBar : StatusBar) {
    this.vehicules = [];
    this.currentVehicules = [];
    this.avance=0;

  }
  add() {
    this.navCtrl.push(VehiculoPage);

  }
  datachanged(vehicule: any, e: any) {
    if (e.checked)
      this.currentVehicules.push(vehicule);
    else
      delete this.currentVehicules[vehicule];

  }

  details(vehicule) {
    this.navCtrl.push(VehiculoDetailPage, { "current": vehicule.vehiculo, "readonly": true, "consumoPersonalizado": vehicule.consumoPersonalizado });
  }

  delete() {
    this.storage.deleteAll(this.currentVehicules)
      .map(x => x * 100)
      .retry(3)
      .subscribe(s => {
        this.avance = s;
      }, s => console.log, () => {
        this.avance = 0;
        this.storage.getAll().subscribe(r => {
          this.vehicules = r;
        })
      })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyvehiculesPage');
  }
  ionViewDidEnter() {
    this.statusBar.show();
    this.storage.getAll()
      .retry(3)
      .subscribe(s => {
        this.vehicules = s;
      });
  }
  

}
