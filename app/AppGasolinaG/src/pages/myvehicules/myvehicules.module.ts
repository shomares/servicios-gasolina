import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyvehiculesPage } from './myvehicules';

@NgModule({
  declarations: [
    MyvehiculesPage,
  ],
  imports: [
    IonicPageModule.forChild(MyvehiculesPage),
  ],
})
export class MyvehiculesPageModule {}
