import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {SesionService} from '../../services/sesionService';

/**
 * Generated class for the PerfilPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-perfil',
  templateUrl: 'perfil.html',
})
export class PerfilPage {
  public perfil:any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private sesionService: SesionService) {
    this.perfil= {
      picture : '',
      first_name: '',
      last_name: '',
      email: ''

    }
    
    this.sesionService.getUser()
    .retry(3)
    .subscribe(s=>{
      this.perfil= s;
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PerfilPage');
   
  }

}
