import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams , ModalController, ViewController} from 'ionic-angular';
import {VehiculosService} from '../../services/vehiculosService';
import { StorageService } from '../../services/storageService';
import { VehiculoDetailPage } from '../vehiculo-detail/vehiculo-detail';

/**
 * Generated class for the VehiculoPageModal page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@Component({
  selector: 'page-vehiculo-page-modal',
  templateUrl: 'vehiculo-page-modal.html',
})
export class VehiculoPageModal  {
      private idMarca:string;
      private idModelo:string;
      private ano:string;
      private vehicules: any[];
      private vehiculesCurrent: any[];
  constructor(public navCtrl: NavController, public navParams: NavParams, private vehiculosService:VehiculosService, public viewCtrl: ViewController,
      private storageService : StorageService
  ) {
        this.idMarca= navParams.get("idMarca");
        this.idModelo= navParams.get("idModelo");
        this.ano= navParams.get("ano");
        this.vehiculesCurrent=[];
        this.vehiculosService.getVehicule(this.idMarca, this.idModelo, this.ano).subscribe(s=>{
          this.vehicules=s;
        });
  }
details(vehicule){ 
      console.log(vehicule);
      this.navCtrl.push(VehiculoDetailPage, {"current": vehicule, "readonly" : false, "consumoPersonalizado": null});
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VehiculoPageModal');
  }


}
