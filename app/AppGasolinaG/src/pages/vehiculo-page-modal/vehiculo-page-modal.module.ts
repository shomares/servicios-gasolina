import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VehiculoPageModal } from './vehiculo-page-modal';

@NgModule({
  declarations: [
    VehiculoPageModal,
  ],
  imports: [
    IonicPageModule.forChild(VehiculoPageModal),
  ],
})
export class VehiculoPageModalModule {}
