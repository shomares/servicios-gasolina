import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { StorageService } from '../../services/storageService';
import { ResultsDetailPage } from '../results-detail/results-detail';
import { StatusBar } from '@ionic-native/status-bar';

/**
 * Generated class for the ResultsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-results',
  templateUrl: 'results.html',
})
export class ResultsPage {

  vehicules: any[];
  currentRoute: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private storage: StorageService,
    private statusBar : StatusBar ) {
    this.currentRoute= navParams.get('current'); 
    this.vehicules= [];
    storage.getAll()
    .retry(3)
    .subscribe(r=>{
        this.vehicules= r;
     });
  }
  changeVehiculo(vehicule){
     this.navCtrl.push(ResultsDetailPage, {currentRoute: this.currentRoute, currentVehicule: vehicule});
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ResultsPage');
  }
  ionViewDidEnter() {
    this.statusBar.show();
  }

}
