import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, Events } from 'ionic-angular';
import { Geolocation, Geoposition } from '@ionic-native/geolocation';
import { DirectionService } from '../../services/directionService';
import { elementAt } from 'rxjs/operators';
import { ResultsPage } from '../results/results';
import { StatusBar } from '@ionic-native/status-bar';
import { Observable } from 'rxjs/Observable';
import { BusquedaPage } from '../busqueda/busqueda';


declare var google;
/**
 * Generated class for the MapaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mapa',
  templateUrl: 'mapa.html',
})
export class MapaPage {

  private map: any;
  private directionsDisplay: any;
  private markers: any[];
  private route: any[];
  private currentPlace: Observable<any>;
  private bounds: any;


  constructor(public navCtrl: NavController, public navParams: NavParams, private geolocation: Geolocation,
    private directionService: DirectionService, public alertCtrl: AlertController, private statusBar: StatusBar,
    public events: Events,  private zone: NgZone

  ) {
    this.markers = [];
    this.route = [];
    this.bounds = new google.maps.LatLngBounds();
    this.events.subscribe("setPlace", s => {
      this.setMarker(s.location.lat(), s.location.lng());
    });
  }

  createMap() {
    let mapEle: HTMLElement = document.getElementById('mapa_canvas');
    let myLatLng = { lat: 23.634501, lng: -102.55278399999997 };
    this.directionsDisplay = new google.maps.DirectionsRenderer();

    this.map = new google.maps.Map(mapEle, {
      center: myLatLng,
      zoom: 8
    });
    google.maps.event.addListenerOnce(this.map, 'idle', () => {
      mapEle.classList.add('show-map');
      this.directionsDisplay.setMap(this.map);
    });
    this.map.addListener('click', (e) => {
      this.setMarker(e.latLng.lat(), e.latLng.lng());
    });
  }
  setMarker(latitude: number, longitude: number) {
    let myLatLng = { lat: latitude, lng: longitude };
    let marker = new google.maps.Marker({
      position: myLatLng,
      map: this.map
    });
    marker.addListener('click', () => {
      marker.setMap(null);
      let gas = this.markers.find(s => s.marker == marker);
      if (gas != null) {
        gas.gasMarkers.forEach(element => {
          element.setMap(null);
        });
        delete this.markers[gas];
      }
    });
    this.markers.push({ marker: marker, gasMarkers: [] });
    this.autoFill(marker.position);


    if (this.markers.length > 1)
      return;

    this.directionService.getGasolineras({ x: longitude, y: latitude })
      .retry(2)
      .subscribe((s: any[]) => {
        let markergasA = this.markers.find(s => s.marker == marker);
        if (markergasA !== null) {
          s.forEach((gas) => {
            let myLatLngGas = { lat: parseFloat(gas.y), lng: parseFloat(gas.x) };
            let posGas = new google.maps.LatLng(myLatLngGas.lat, myLatLngGas.lng);
            let markergas = new google.maps.Marker({
              position: posGas,
              map: this.map,
              icon: "assets/imgs/fillingstation.png",
            });
            markergas.setMap(this.map);
            markergasA.gasMarkers.push(markergas);
          });
        }
      });

  }

  setPosition() {
    let position = this.geolocation.getCurrentPosition();
    position.then((pos) => {
      this.setMarker(pos.coords.latitude, pos.coords.longitude);
    }).catch(console.log);
  }

  calculate() {
    let wayspoints = [];
    if (this.markers.length > 1) {
      let alert = this.alertCtrl.create({
        title: 'Calcular',
        message: 'Desea pasar por alguna gasolinera?',
        buttons: [
          {
            text: 'No',
            role: 'cancel',
            handler: () => {
              this.calculateDirections();
            }
          },
          {
            text: 'Si',
            handler: () => {
              this.setEventsAssinGas();
              this.alertCtrl.create({
                title: 'Vehiculos',
                subTitle: 'Seleccione alguna gasolineria',
                buttons: ['OK']
              }).present();
            }
          }
        ]
      });
      alert.present();
    }
  }

  setEventsAssinGas() {
    let marker = this.markers[0];
    marker.gasMarkers.forEach(element => {
      element.addListener('click', () => {
        this.asignarGasolinera(element);
      });
    });
  }

  asignarGasolinera(gas) {
    this.markers.splice(1, 0, { gasMarkers: [], marker: gas });
    this.calculateDirections();
  }

  calculateDirections() {
    this.directionsDisplay.setMap(this.map);
    this.directionService.calculteRoute(this.markers.map(s => s.marker))
      .retry(3)
      .subscribe((route) => {
        let legs = route.legs;
        legs.forEach(leg => {
          let steps = leg.steps;
          steps.forEach(step => {
            var nextSegment = step.path;
            var stepPolyline = new google.maps.Polyline({
              strokeColor: '#C83939',
              strokeOpacity: 1,
              strokeWeight: 4
            });
            nextSegment.forEach(segment => {
              stepPolyline.getPath().push(segment);
            });
            stepPolyline.setMap(this.map);
            stepPolyline.addListener('click', () => {
              this.navCtrl.push(ResultsPage, { current: route });
            });
            this.route.push(stepPolyline);
          });
        });
      });
  }

  clean() {
    this.markers.forEach(r => {
      r.gasMarkers.forEach(s => {
        s.setMap(null);
      });
      r.marker.setMap(null);
    });
    this.route.forEach(s => {
      s.setMap(null);
    });
    this.route = [];
    this.markers = [];
    this.directionsDisplay.setMap(null);
    this.bounds = new google.maps.LatLngBounds();

  }

  search() {
    this.navCtrl.push(BusquedaPage);
  }

  autoFill(center) {
      this.bounds.extend(center);

      setTimeout(()=>{
        this.map.fitBounds(this.bounds);
      }, 100);

  }
  ionViewDidLoad() {
    this.createMap();
  }
  ionViewDidEnter() {
    this.statusBar.hide();
  }


}
