import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController } from 'ionic-angular';
import {VehiculosService} from '../../services/vehiculosService';
import {VehiculoPageModal} from '../vehiculo-page-modal/vehiculo-page-modal';
import {StorageService} from '../../services/storageService';
/**
 * Generated class for the VehiculoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-vehiculo',
  templateUrl: 'vehiculo.html',
})
export class VehiculoPage {
  anos: any[];
  private marcas;
  private currentMarca;
  private modelos;
  private currentAno;
  private currentModelo;
  constructor(public navCtrl: NavController, public navParams: NavParams, private vehiculosService:VehiculosService,
    private storageService: StorageService, private alertCtrl: AlertController
  ) {
         vehiculosService.getMarcas().subscribe(data=>{
          this.marcas= data;
        });
  }
  changeMarca(){
      this.vehiculosService.getModeloByMarca(this.currentMarca.value)
      .retry(3)
      .subscribe(data=>{
        this.modelos= data;
      });
  }
  changeModelo(){
    this.vehiculosService.getanobyModelo(this.currentMarca.value, this.currentModelo.value)
    .retry(3)
    .subscribe(data=>{
      this.anos= data;
    });
    
  }
  find(){
    let obj= {idMarca: this.currentMarca.value, idModelo: this.currentModelo.value, ano: this.currentAno.value};
    this.navCtrl.push(VehiculoPageModal,obj);
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad VehiculoPage');
  }
}

