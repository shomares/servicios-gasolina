import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import {Marca} from '../beans/marcas';
import {SettingService} from '../services/settingService';
import {SesionService} from '../services/sesionService';



@Injectable()
export class VehiculosService{

    constructor(private http:Http, private settingService: SettingService, private session : SesionService){
   
    }
   
    getMarcas(): Observable<any[]>{
        console.log(this.session.credential.access_token);
        let headersa = new Headers({'Authorization': 'Bearer ' + this.session.credential.access_token });
            return this.http.get(this.settingService.url + 'marcas', {
                   headers: headersa 
            }).
            map( (r:Response)=>r.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }
    getModeloByMarca(marca:string) :Observable<any[]>{
        console.log(this.session.credential.access_token);
        let headersa = new Headers({'Authorization': 'Bearer ' + this.session.credential.access_token });
        return this.http.get(this.settingService.url +  'modelos/' + marca, {
            headers: headersa 
        }).map( (r:Response)=>r.json().SubModelos)
        .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }
    getanobyModelo(marca:string, modelo:string): Observable<any[]>{
        console.log(this.session.credential.access_token);
        let headersa = new Headers({'Authorization': 'Bearer ' + this.session.credential.access_token });

        return this.http.get(this.settingService.url +  'ano/' + marca + '/' + modelo, {
            headers: headersa 
        }).map( (r:Response)=>r.json())
        .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }
    getVehicule(marca:string, modelo:string, ano:string) : Observable<any[]>{
        console.log(this.session.credential.access_token);
        let headersa = new Headers({'Authorization': 'Bearer ' + this.session.credential.access_token });
        return this.http.get( this.settingService.url + "vehiculo/" + marca + "/" + modelo + "/" + ano, {
            headers: headersa 
        }).map( (r:Response)=>r.json())
        .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }
}