import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import {SettingService} from '../services/settingService';
import { SesionService } from './sesionService';
import { timeInterval } from 'rxjs/operators';

declare var google;

@Injectable()
export class DirectionService{
    directionsService = new google.maps.DirectionsService();
    geocoder = new google.maps.Geocoder();
    currentPlace: any[];

  
    constructor(private http:Http, private settingService: SettingService, 
        private session : SesionService){
            this.currentPlace= [];
    }

    calculteRoute(markers:any[]): Observable<any>{
        return Observable.create(resolve=>{
            let wayspoints=[];
            markers.forEach(element => {
              wayspoints.push( {location: {lat: element.position.lat(),
                 lng: element.position.lng()}, stopover: true});
            });
            let request = {
              origin:wayspoints[0].location,
              destination: wayspoints[wayspoints.length - 1].location,
              waypoints: wayspoints,
              travelMode: 'DRIVING',
              drivingOptions: {
                  departureTime: new Date(Date.now()),
                  trafficModel: 'optimistic'
              },
              provideRouteAlternatives: true
            };
            this.directionsService.route(request, (response, status)=>{
                    response.routes.forEach(element => {
                        setTimeout(()=>{
                            resolve.next(element);
                          }, 1000)
                    });
            });
        });
    }
    getGasolineras(location:any): Observable<any[]>{
        let headersa = new Headers({'Authorization': 'Bearer ' + this.session.credential.access_token });
        return this.http.get(this.settingService.url + 'gas/' + location.x + '/' + location.y, {headers: headersa} ).map( (r:Response)=>r.json())
        .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }

    findPlaces(location:string): Observable<any[]>{
        return Observable.create(observer=>{
            this.geocoder.geocode({'address': location}, (result, status)=>{
                    observer.next(result);
            });
        });
    }

    getCurrentPosition():Observable<any>{
        return Observable.create(observable=>{
            navigator.geolocation.getCurrentPosition(position=>{
                observable.next(position);
            }, error=>{
                observable.error(error);
            });
        });



    }




}