import { Injectable } from '@angular/core';
import {Http, Headers, RequestOptions, Response} from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SettingService } from './settingService';

@Injectable()
export class SesionService{
 
    public credential : any;

    constructor(private http: Http, private settingService: SettingService){

    }

    setCredentials(data: any){
        this.credential= data;
    }

    getUser(): Observable<any>{
        let headersa = new Headers({'Authorization': 'Bearer ' + this.credential.access_token});
        return this.http.get(this.settingService.url + 'me', {
            headers: headersa 
     }).map( (r:Response)=>r.json())
     .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }
}
