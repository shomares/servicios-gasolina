
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { SettingService } from '../services/settingService';
import { SesionService } from '../services/sesionService';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Rx';


@Injectable()
export class StorageService {

    constructor(private storage: Storage, private http: Http, private settingService: SettingService, private session: SesionService) { }


    getAll(): Observable<any[]> {
        let headersa = new Headers({ 'Authorization': 'Bearer ' + this.session.credential.access_token });
        return this.http.get(this.settingService.url + 'myvehicules', {
            headers: headersa
        }).map((r: Response) => r.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }

    isEmpty(): Observable<boolean> {
        let values = this.getAll();
        return Observable.create(resolve => {
            values.subscribe(s => {
                if (s.length > 0)
                    resolve.next(false);
                else
                    resolve.next(true);
            });
        });
    }

    saveItem(data: any): Observable<any> {
        let headersa = new Headers({ 'Authorization': 'Bearer ' + this.session.credential.access_token });
        return this.http.post(this.settingService.url + 'myvehicules', data, {
            headers: headersa
        }).map((r: Response) => r.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }

    save(data: any[]): Observable<any> {
        let size = data.length;
        let i = 0;
        return Observable.create(observer => {
            data.forEach(s => {
                let data = this.saveItem(s);
                data.subscribe(result => {
                    observer.next(i/size);
                    i++;
                    if (i >= size) {
                        observer.complete();
                    }
                })
            });
        });
    }

    get(id: any): Observable<any> {
        let headersa = new Headers({ 'Authorization': 'Bearer ' + this.session.credential.access_token });
        return this.http.get(this.settingService.url + 'myvehicules/' + id, {
            headers: headersa
        }).map((r: Response) => r.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }

    deleteAll(data: any[]): Observable<any> {
        let size = data.length;
        let i = 0;
        return Observable.create(observer => {
            data.forEach(s => {
                let data = this.delete(s);
                data.subscribe(result => {
                    observer.next(i/size);
                    i++;
                    if (i >= size) {
                        observer.complete();
                    }
                })
            });
        });
    }

    delete(id: any): Observable<any> {
        let headersa = new Headers({ 'Authorization': 'Bearer ' + this.session.credential.access_token });
        return this.http.delete(this.settingService.url + 'myvehicules/' + JSON.stringify(id), {
            headers: headersa
        }).map((r: Response) => r.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }
    saveRoute(route:any):Observable<any>{
        let headersa = new Headers({ 'Authorization': 'Bearer ' + this.session.credential.access_token });
        return this.http.post(this.settingService.url + 'route/save',route ,{
            headers: headersa
        }).map((r: Response) => r.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));

    }


}