import { SettingService } from "./settingService";
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { InAppBrowser } from '@ionic-native/in-app-browser';

@Injectable()
export class LoginService {

    constructor(private settingService: SettingService, private iab: InAppBrowser) {
    }

    login(): Observable<any> {
        return Observable.create(observable => {
            let browserRef = this.iab.create(this.settingService.urlLogin);
            browserRef.on("loadstart").subscribe(event => {
                if ((event.url).indexOf(this.settingService.urlRedirect) === 0) {
                    browserRef.close();
                    var responseParameters = ((event.url).split("?")[1]).split("#");
                    var parsedResponse = {};
                    for (var i = 0; i < responseParameters.length; i++) {
                        parsedResponse[responseParameters[i].split("=")[0]] = responseParameters[i].split("=")[1];
                    }
                    if (parsedResponse["access_token"] !== undefined && parsedResponse["access_token"] !== null) {
                        observable.next(parsedResponse["access_token"]);
                    } else {
                        observable.error("Problem authenticating with api");
                    }

                }
            });
        });
    }

    loginBrowser(): Observable<any>{
        return Observable.create(resolve=>{
            let ref= window.open(this.settingService.urlLogin, '', "width=400,height=4|00");
            window.onmessage= function(e){
                ref.close();    
                resolve.next(e.data);
                resolve.complete();
            }
        });
    }
}