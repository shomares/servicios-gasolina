import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { IonicStorageModule } from '@ionic/storage';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpModule } from '@angular/http';
import { Geolocation } from '@ionic-native/geolocation';
import {ProgressBarModule} from "angular-progress-bar";
import { InAppBrowser } from '@ionic-native/in-app-browser';

import { MyApp } from './app.component';
import { MapaPage } from '../pages/mapa/mapa';
import {VehiculoPage} from '../pages/vehiculo/vehiculo';
import {VehiculoPageModal} from '../pages/vehiculo-page-modal/vehiculo-page-modal';
import {PerfilPage} from '../pages/perfil/perfil';
import {MyvehiculesPage} from '../pages/myvehicules/myvehicules';

import {VehiculosService} from '../services/vehiculosService';
import {StorageService} from '../services/storageService';
import {DirectionService} from '../services/directionService';
import {SettingService} from '../services/settingService';
import { LoginService } from '../services/loginService';
import { LoginPage } from '../pages/login/login';
import { SesionService } from '../services/sesionService';
import { VehiculoDetailPage } from '../pages/vehiculo-detail/vehiculo-detail';
import { ResultsPage } from '../pages/results/results';
import { ResultsDetailPage } from '../pages/results-detail/results-detail';
import { BusquedaPage } from '../pages/busqueda/busqueda';



@NgModule({
  declarations: [
    MyApp,
    MapaPage,
    VehiculoPage,
    LoginPage,
    VehiculoPageModal,
    PerfilPage,
    MyvehiculesPage,
    VehiculoDetailPage,
    ResultsDetailPage,
    ResultsPage,
    BusquedaPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    HttpModule ,
    ProgressBarModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    MapaPage,
    VehiculoPage,
    LoginPage,
    VehiculoPageModal,
    PerfilPage,
    ResultsDetailPage,
    MyvehiculesPage,
    VehiculoDetailPage, 
    ResultsPage,
    BusquedaPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    VehiculosService,
    LoginService,
    StorageService,
    Geolocation,
    SettingService,
    DirectionService,
    SesionService,
    InAppBrowser

    
  ]
})
export class AppModule {}
