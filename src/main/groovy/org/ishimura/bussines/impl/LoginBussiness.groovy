package org.ishimura.bussines.impl

import org.ishimura.bussines.ILoginBussines
import javax.inject.Inject
import org.ishimura.dao.FactoryRepository
import org.ishimura.dao.UsuarioRepository
import org.ishimura.entities.Usuario

class LoginBussiness implements ILoginBussines{

	FactoryRepository factoryRepository

	@Inject
	LoginBussiness(FactoryRepository repository){
		this.factoryRepository= repository
	}


	void login(String username){
		Usuario user=null

		def repository = factoryRepository.createUsuarioRepository()
		try{
			user= repository.getUser(username)
			if(user==null){
				user= new Usuario()
				user.correo= username
				repository.saveUser(user)
			}
			repository.close()
		}catch(Exception ex){
			println (ex)
		}
	}
}
