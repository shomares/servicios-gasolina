package org.ishimura.bussines.impl

import javax.inject.Inject;

import org.bson.types.ObjectId
import org.ishimura.bussines.IRegistroBussines
import org.ishimura.dao.FactoryRepository;
import org.ishimura.entities.Registro

class RegistroBussines implements IRegistroBussines {
	FactoryRepository factoryRepository

	@Inject
	RegistroBussines(FactoryRepository repository){
		this.factoryRepository= repository
	}


	@Override
	public Object getAllByUser(String username) {
		// TODO Auto-generated method stub
		def repository = factoryRepository.createRegistroRepositroy()
		def lista= repository.getRegistroByUser(username)
		repository.close()
		return lista;
	}

	@Override
	public void  save(String username, Object data) {
		// TODO Auto-generated method stub
		def repository = factoryRepository.createRegistroRepositroy()
		def repositoryUser= factoryRepository.createUsuarioRepository()
		
		
		Registro reg= new Registro()
		
	
		reg.fecha = new Date()
		reg.consumoPersonalizado = data.consumoPersonalizado
		reg.vehiculo= data.vehiculo
		reg.vehiculo.id=  "${reg.vehiculo.marca}-${reg.vehiculo.nombre}"
		
		reg.usuario = repositoryUser.getUser(username)
		repository.saveRegistro(reg)

		repository.close()
		repositoryUser.close()
	}

	@Override
	public void delete(String username, data) {
		// TODO Auto-generated method stub
		def repository = factoryRepository.createRegistroRepositroy()
		repository.delete(username, data)
		repository.close()
	}

	@Override
	public Object get(ObjectId data) {
		// TODO Auto-generated method stub
		def repository = factoryRepository.createRegistroRepositroy()
		def salida= repository.get(data)
		repository.close()
		return salida
	}


	@Override
	public void saveRoute(def route) {
		// TODO Auto-generated method stub
		def repository = factoryRepository.createRegistroRepositroy()
		def salida= repository.saveRoute(route)
		repository.close()
		
	}



	
}
