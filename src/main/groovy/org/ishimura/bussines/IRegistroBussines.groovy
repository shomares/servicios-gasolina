package org.ishimura.bussines

import org.bson.types.ObjectId

interface IRegistroBussines {
	def getAllByUser(String username)
	void save(String username, data)
	void delete(String username,  data)
	def get(ObjectId data) 
	void saveRoute(def route)
	

}
