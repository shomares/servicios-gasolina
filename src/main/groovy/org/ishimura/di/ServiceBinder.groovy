package org.ishimura.di

import com.google.inject.AbstractModule
import com.google.inject.Provides
import org.ishimura.bussines.*
import org.ishimura.bussines.impl.*
import com.google.inject.Singleton
import io.vertx.core.Context
import io.vertx.core.Vertx
import org.ishimura.interfaces.IAnalizador
import org.ishimura.interfaces.IDaoCatalogo
import org.ishimura.interfaces.IDaoGasolinera;
import org.ishimura.interfaces.IRepository
import org.ishimura.webspider.AnalizadorHTTP
import org.ishimura.webspider.DaoCatalogosHTTP
import org.ishimura.webspider.DaoGasolinera
import org.ishimura.webspider.RedisRepository
import org.ishimura.dao.*
import org.ishimura.dao.impl.*

class ServiceBinder extends AbstractModule implements FactoryRepository {
	@Override
	protected void configure() {
		bind(IAnalizador.class).to(AnalizadorHTTP.class)
		bind(IDaoCatalogo.class).to(DaoCatalogosHTTP.class)
		bind(IDaoGasolinera.class).to(DaoGasolinera.class)
		bind(IRepository.class).to(RedisRepository.class)
		bind(ILoginBussines.class).to(LoginBussiness.class)
		bind(FactoryRepository.class).to(ServiceBinder.class)
		bind(IRegistroBussines.class).to(RegistroBussines.class)
	}

	@Override
	public RegistroRepository createRegistroRepositroy() {
		// TODO Auto-generated method stub
		return new RegistroMongoRepository();
	}

	@Override
	public UsuarioRepository createUsuarioRepository() {
		// TODO Auto-generated method stub
		return new UsuarioMongoRepository();
	}
}
