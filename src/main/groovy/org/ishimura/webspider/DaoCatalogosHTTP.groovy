package org.ishimura.webspider

import javax.inject.Inject

import com.squareup.okhttp.FormEncodingBuilder
import com.squareup.okhttp.OkHttpClient
import com.squareup.okhttp.Request
import com.squareup.okhttp.RequestBody
import com.squareup.okhttp.Response
import java.util.List

import org.ishimura.beans.Ano
import org.ishimura.beans.Marca
import org.ishimura.beans.SubModelo
import org.ishimura.beans.Vehiculo
import org.ishimura.interfaces.IAnalizador
import org.ishimura.interfaces.IDaoCatalogo

class DaoCatalogosHTTP implements IDaoCatalogo{
	IAnalizador analizador;

	@Inject
	DaoCatalogosHTTP(IAnalizador analizador){
		this.analizador = analizador  ;
	}

	@Override
	public List<Marca> getMarcas() {
		// TODO Auto-generated method stub
		def request= "http://www.ecovehiculos.gob.mx/buscamarcamodelo.php".toURL()
				.openStream();
		return analizador.getList("marca_id", request)
	}

	@Override
	public List<SubModelo> getSubModelos(String marca) {
		// TODO Auto-generated method stub
		def request= "http://www.ecovehiculos.gob.mx/buscamarcamodelo.php?marca_id=${marca}".toURL()
				.openStream();
		return analizador.getList("vehiculo_submarca", request)
	}

	@Override
	public List<Ano> getAnos(String marca, String subModelo) {
		def request= "http://www.ecovehiculos.gob.mx/buscamarcamodelo.php?marca_id=${marca}&vehiculo_submarca=${subModelo}".toURL()
				.openStream();
		return analizador.getList("vehiculo_modelo", request)
	}

	@Override
	public List<Vehiculo> getVehiculos(String marca, String subModelo, String ano) {
		// TODO Auto-generated method stub

		OkHttpClient client = new OkHttpClient();
		List<Vehiculo> salida = null;

		RequestBody formBody = new FormEncodingBuilder().
				add("marca_id", marca)
				.add("vehiculo_modelo",  ano)
				.add("vehiculo_submarca", subModelo)
				.build();
		Request request = new Request.Builder().url("http://www.ecovehiculos.gob.mx/buscamarcamodelo2.php")
				.post(formBody).addHeader("origin", "http://www.ecovehiculos.gob.mx")
				.addHeader("upgrade-insecure-requests", "1")
				.addHeader("user-agent",
				"Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36")
				.addHeader("content-type", "application/x-www-form-urlencoded")
				.addHeader("accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8")
				.addHeader("referer",
				"http://www.ecovehiculos.gob.mx/buscamarcamodelo.php?marca_id="+  marca +"&vehiculo_submarca=" +  subModelo)
				.addHeader("accept-encoding", "gzip, deflate").addHeader("accept-language", "es,en-US;q=0.8,en;q=0.6")
				.addHeader("cookie",
				"__utmt=1; __utma=21458479.1460983810.1490377859.1490377859.1490464944.2; __utmb=21458479.3.10.1490464944; __utmc=21458479; __utmz=21458479.1490377859.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)")
				.addHeader("cache-control", "no-cache")
				.addHeader("postman-token", "1a7c3ddd-1ce1-a46d-3206-05791df887b2").build();

		Response response = client.newCall(request).execute();
		if (response.isSuccessful()) {
			InputStream body = response.body().byteStream();
			return analizador.getListFromTable(body, {pila->

				Vehiculo vehiculo= new Vehiculo();
				String args= pila.pop().replaceAll("&nbsp;>", "").replaceAll(">", "");
				vehiculo.setEmisionNOX(Double.valueOf(args));
				args= pila.pop().replaceAll("&nbsp;>", "").replaceAll(">", "");
				vehiculo.setEmision(Double.valueOf(args));
				args= pila.pop().replaceAll("&nbsp;>", "").replaceAll(">", "");
				vehiculo.setRendimientoPorLitro(Double.valueOf(args));
				args= pila.pop().replaceAll("&nbsp;>", "").replaceAll(">", "");
				vehiculo.setCombustible(args);
				args= pila.pop().replaceAll("&nbsp;>", "").replaceAll(">", "");
				vehiculo.setTransmision(args);
				args= pila.pop().replaceAll("&nbsp;>", "").replaceAll(">", "");
				vehiculo.setAno(args);
				args= pila.pop().replaceAll("&nbsp;>", "").replaceAll(">", "");
				vehiculo.setNombre(args);
				vehiculo.setMarca(marca);

				return vehiculo;

			}, "tabla_ecoetiqEF", "tabla_ecoetiqGH", "tabla_ecoetiqIJ", "tabla_ecoetiqNO",
			"tabla_ecoetiqLM", "tabla_ecoetiqPQ")

		}
	}
}
