package org.ishimura.webspider


import javax.inject.Inject

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx
import io.vertx.redis.RedisClient
import io.vertx.redis.RedisOptions
import io.vertx.redis.op.GeoUnit
import io.vertx.core.Future
import io.vertx.core.json.JsonObject

import java.util.List;

import groovy.json.JsonOutput

import org.ishimura.beans.GasolineraDTO;
import org.ishimura.beans.PrecioDTO;
import org.ishimura.interfaces.IDaoGasolinera;

class DaoGasolinera implements IDaoGasolinera {

	private Vertx vertx
	private RedisClient client
	private RedisOptions config

	@Inject
	DaoGasolinera(Vertx vertx){
		this.vertx= vertx
		config = new RedisOptions()
				.setAuth("p6aa1829928d2bdc41a3acb873924d125fb8bda9b4f9c4c8b4fa125beda8afb03")
				.setHost("ec2-34-235-90-46.compute-1.amazonaws.com")
				.setPort(13659)
		this.client= RedisClient.create(vertx, config)
	}

	@Override
	public void init() {
		// TODO Auto-generated method stub
		def request= "https://publicacionexterna.azurewebsites.net/publicaciones/places".toURL()
		def data =  new XmlParser().parse(request.openStream())
		def sizes= data.place.size()
		def i=0
		data.place.each{ r->
			def gasolineria =[
				id: r.@place_id,
				adress: r.location.address_street.text(),
				name:r.name.text(),
				brand: r.brand.text(),
				cre_id: r.cre_id.text(),
				x: Double.parseDouble(r.location.x.text()),
				y:  Double.parseDouble(r.location.y.text())
			]
			client.geoadd("gasolinerias", gasolineria.x, gasolineria.y, JsonOutput.toJson(gasolineria) ,{s->
				println ("Se ha actualizado las gasolineras")
			})
		}
	}


}
