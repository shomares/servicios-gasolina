package org.ishimura.webspider

import java.io.InputStream

import org.ishimura.interfaces.IAnalizador

class AnalizadorHTTP implements IAnalizador{

	def getBuffer(InputStream stream) {
		return new BufferedReader(new InputStreamReader(stream));
	}
	@Override
	def getList(String id, InputStream body) {
		String thisLine = "";
		def salida = [];
		BufferedReader br = getBuffer(body);
		while ((thisLine = br.readLine()) != null) {
			String absucar = "<select name=" + '"' + id;
			if (thisLine.contains(absucar)) {
				while (!(thisLine = br.readLine()).contains("</select>")) {
					if (thisLine.contains("option")) {
						String token = "", value = "", text = "";
						for (int i = 0; i < thisLine.length(); i++) {
							char aux = thisLine.charAt(i);
							if (token.contains("=")) {
								aux = thisLine.charAt(++i);
								while (aux != '"') {
									value += aux;
									aux = thisLine.charAt(++i);
								}
								aux = thisLine.charAt(++i);
								while (aux != '<') {
									if (aux != '>')
										text += aux;
									aux = thisLine.charAt(++i);
								}
								if (!value.isEmpty()) {
									def obj = [text:text, value: value];
									salida.add(obj);
								}
								token = "";
								value = "";
								text = "";
								break;
							}
							if (aux == '=') {
								token += aux;
							}
						}
					}
				}
			}
		}
		return salida;
	}

	String getValueTextFromLabel(String label, String text) {
		String salida = "";
		int i = 0, j = 1;
		char aux = text.charAt(i);

		while (i < text.length() && aux != '>') {
			aux = text.charAt(++i);
			j++;
		}
		salida = text.substring(j, text.length()).replaceAll("</" + label, "");
		if (salida.contains("</") && !salida.isEmpty()) {
			i = salida.length() - 1;
			aux = salida.charAt(i);
			while (aux != '<') {
				aux = salida.charAt(--i);
			}

			label = salida.substring(i, salida.length());
			i = 0;
			while (aux != '>')
				aux = label.charAt(++i);
			label = label.substring(0, i).replaceAll("</", "").replaceAll(">", "");

			return getValueTextFromLabel(label, salida);
		} else {
			return salida;
		}
	}

	boolean containList(String[] lista, String source) {
		return lista.any{elemento ->
			if(source.contains(elemento)) {
				return true
			}
			return
		};
	}
	@Override
	def getListFromTable(InputStream response, closure, String... id) {
		BufferedReader br = getBuffer(response);
		def salida = [];
		def c= closure.clone();
		String thisLine = "";
		Stack<String> pila = new Stack<String>();

		while ((thisLine = br.readLine()) != null) {
			if (thisLine.contains("<table")) {
				while (!thisLine.contains("</table>")) {
					if (thisLine.contains("<tr") && containList(id, thisLine)) {
						// Estamos en los <TR>
						if (pila != null)
							pila.clear();
						pila = new Stack<String>();
						while (!thisLine.contains("</tr>")) {
							thisLine = br.readLine();
							if (thisLine.contains("<td")) {
								String value = getValueTextFromLabel("td", thisLine);
								if (!value.isEmpty())
									pila.push(value);
							}
						}
						def obj = c(pila);
						if (obj != null)
							salida.add(obj);
					}
					thisLine = br.readLine();
				}
			}
		}

		return salida;
	}

}

