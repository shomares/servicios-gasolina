package org.ishimura.webspider

import javax.inject.Inject

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.ObjectWriter

import groovy.json.JsonSlurper
import groovy.swing.binding.JScrollBarProperties
import groovy.json.JsonOutput
import io.vertx.core.AsyncResult
import io.vertx.core.Handler
import io.vertx.core.Vertx
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.core.Future
import io.vertx.redis.RedisClient
import io.vertx.redis.RedisOptions
import io.vertx.redis.op.GeoUnit
import org.ishimura.beans.Marca
import org.ishimura.interfaces.IDaoCatalogo
import org.ishimura.interfaces.IRepository
class RedisRepository implements IRepository {

	Vertx vertx
	RedisOptions config
	IDaoCatalogo dao
	RedisClient client

	@Inject
	RedisRepository(Vertx vertx, IDaoCatalogo dao){
		config = new RedisOptions()
				.setAuth("p6aa1829928d2bdc41a3acb873924d125fb8bda9b4f9c4c8b4fa125beda8afb03")
				.setHost("ec2-34-235-90-46.compute-1.amazonaws.com")
				.setPort(13659)
		this.vertx=vertx
		this.dao= dao
		this.client= RedisClient.create(vertx, config)
	}


	@Override
	public void save(marca) {
		def  ow = new ObjectMapper().writer().withDefaultPrettyPrinter()
		def marcaStr= ow.writeValueAsString(marca)
		//Validate not exist
		this.client.exists(EnumStatic.KEYMARCAS, {r->
			if(r.succeeded()) {
				this.client.hset(EnumStatic.KEYMARCAS,marca.value, marcaStr, {s->
					if(s.succeeded()){
						println("Se ha guardado correctamente")
					}else{
						println("Error")
					}
				})
			}
		})
	}


	@Override
	void init(Handler<AsyncResult<Boolean>> next) {
		this.client.exists(EnumStatic.KEYMARCAS, {r->
			if (r.succeeded()) {
				next.handle(Future.succeededFuture(true))
			}else {
				next.handle(Future.succeededFuture(true))
			}
		});
	}

	@Override
	void getMarca(String id,  Handler<AsyncResult<String>> next) {
		this.client.hget(EnumStatic.KEYMARCAS, id, {r->
			if (r.succeeded())
			{
				next.handle(Future.succeededFuture(	r.result()))
			}
			else{
				next.handle(Future.failedFuture("NOT FOUND"))
			}
		})
	}
	@Override
	public void getMarcas(Handler<AsyncResult<String>> next) {
		this.client.hgetall(EnumStatic.KEYMARCAS,{r->
			if (r.succeeded()) {
				def jsonSlurper = new JsonSlurper()
				def salida= []
				JsonObject js= r.result()
				js.forEach({s->
					def obj =jsonSlurper.parseText(s.value.toString());
					salida.add([text:obj.text, value:obj.value]);
				})
				next.handle(Future.succeededFuture(JsonOutput.toJson(salida)))
			}else{
				next.handle(next.handle(Future.failedFuture("NOT FOUND")))
			}
		})
	}
	@Override
	public void getAnos(String marca, String submodelo,
			Handler<AsyncResult<String>> next) {
		// TODO Auto-generated method stub
		this.client.hget("${EnumStatic.KEYANOS}-${marca}", submodelo, {r->
			if (r.succeeded()){
				if(r.result()!=null){
					next.handle(Future.succeededFuture(r.result()))
				}else{
					def json = JsonOutput.toJson([marca: marca, submodelo: submodelo])
					vertx.eventBus().send("org.ishimura.implementacion.getAno", json,{s->
						next.handle(Future.succeededFuture(s.result().body().toString()))
					})
				}
			}
			else{
				next.handle(Future.failedFuture("NOT FOUND"))
			}
		})
	}
	@Override
	public void getVehiculo(String marca, String submodelo, String ano,
			Handler<AsyncResult<String>> next) {
		// TODO Auto-generated method stub
		this.client.hget("${EnumStatic.KEYVEHICULOS}-${marca}-${submodelo}", ano, {r->
			if (r.succeeded()){
				if(r.result()!=null){
					next.handle(Future.succeededFuture(r.result()))
				}else{
					def json = JsonOutput.toJson([marca: marca, submodelo: submodelo, ano: ano])
					vertx.eventBus().send("org.ishimura.implementacion.getVehiculo", json, {s->
						next.handle(Future.succeededFuture(s.result().body().toString()))
					})
				}
			}
			else
			{
				next.handle(Future.failedFuture("NOT FOUND"))
			}
		})
	}

	@Override
	public void findGasolineras (double x, double y, Handler<AsyncResult<String>> next) {
		// TODO Auto-generareturn nullted method stub
		this.client.georadius("gasolinerias", x, y, 1.5, GeoUnit.km, {r->
			def js= r.result()
			def salida=[]
			if (r.succeeded()) {
				def jsonSlurper = new JsonSlurper()
				js.forEach({s->
					def obj =jsonSlurper.parseText(s.value.toString());
					salida.add(obj);
				})
				next.handle(Future.succeededFuture(JsonOutput.toJson(salida)))
			}
		})
	}
}
