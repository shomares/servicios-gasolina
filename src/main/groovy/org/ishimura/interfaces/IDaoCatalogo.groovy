package org.ishimura.interfaces

import java.util.List;

import org.ishimura.beans.Ano;
import org.ishimura.beans.Marca;
import org.ishimura.beans.SubModelo;
import org.ishimura.beans.Vehiculo;

interface IDaoCatalogo {

	public List<Marca> getMarcas()
	List<SubModelo> getSubModelos(String marca)
	List<Ano> getAnos(String marca, String subModelo)
	List<Vehiculo> getVehiculos(String marca, String subModelo, String ano)
	
}
