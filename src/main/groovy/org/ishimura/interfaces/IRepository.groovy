package org.ishimura.interfaces

import io.vertx.core.AsyncResult
import io.vertx.core.Handler

import org.ishimura.beans.Marca

interface IRepository {

	void save(marca)
	
	void getMarca(String id,  Handler<AsyncResult<String>> next)
	
	void getMarcas(Handler<AsyncResult<String>> next)
	
	void init(Handler<AsyncResult<Boolean>> next)
	
	void getAnos(String marca, String submodelo, Handler<AsyncResult<String>> next)
	
	void getVehiculo(String marca, String submodelo, String ano, Handler<AsyncResult<String>> next)
	
	void findGasolineras (double x, double y, Handler<AsyncResult<String>> next)
}
