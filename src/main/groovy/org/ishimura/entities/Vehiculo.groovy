package org.ishimura.entities

import org.mongodb.morphia.annotations.Entity
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Property;
import org.mongodb.morphia.annotations.Reference;


@Entity
class Vehiculo {

	@Id
	@Property
	String id
	
	@Property
	String marca
	@Property
	String nombre
	@Property
	String ano
	@Property
	String transmision
	@Property
	String combustible
	@Property
	Double rendimientoPorLitro
	@Property
	Double emision
	@Property
	Double emisionNOX
	
	@Reference
	List<Registro> registros 
}
