package org.ishimura.entities

import org.mongodb.morphia.annotations.Entity
import org.mongodb.morphia.annotations.Id
import org.mongodb.morphia.annotations.Property
import org.mongodb.morphia.annotations.Reference


@Entity
class Usuario {
	
	@Reference
	List<Registro> registros = []
	
	@Id
	@Property
	String correo

}
