package org.ishimura.entities

import org.bson.types.ObjectId
import org.mongodb.morphia.annotations.Entity
import org.mongodb.morphia.annotations.Id
import org.mongodb.morphia.annotations.Property


@Entity
class Punto {
	@Id
	@Property
	ObjectId id

	@Property
	Float longitud

	@Property
	Float latitud

	@Property
	Boolean puntoInicial

	@Property
	Boolean puntoFinal
}
