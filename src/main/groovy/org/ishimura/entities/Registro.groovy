package org.ishimura.entities

import com.fasterxml.jackson.annotation.JsonIgnore
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Embedded
import org.mongodb.morphia.annotations.Entity
import org.mongodb.morphia.annotations.Id
import org.mongodb.morphia.annotations.Property
import org.mongodb.morphia.annotations.Reference

@Entity
class Registro {
	
	
	@Id
	@JsonIgnore
	ObjectId id
	
	@Property
	Date fecha
	
	@Reference
	Usuario usuario
	
	@Reference
	Vehiculo vehiculo
	
	@Property
	Double consumoPersonalizado
	
	@Embedded
	List<Estimado> estimados= []
	
	String objectId
	
	Double consumoPromedio
}
