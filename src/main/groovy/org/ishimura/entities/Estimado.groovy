package org.ishimura.entities
import org.bson.types.ObjectId
import org.mongodb.morphia.annotations.Embedded
import org.mongodb.morphia.annotations.Entity
import org.mongodb.morphia.annotations.Id
import org.mongodb.morphia.annotations.Property
import org.mongodb.morphia.annotations.Reference;


@Entity
class Estimado {

	@Id
	@Property
    ObjectId id
	
   
	@Property
	def consumoEco
	
	@Property
	def consumoPersonalizado
	
	@Property
	def consumoPromedio
	
	@Property
	def distancia
	
    @Property
	Date fecha
	
	@Embedded
	List<Punto> puntos
	
}
