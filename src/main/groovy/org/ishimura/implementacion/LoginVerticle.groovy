package org.ishimura.implementacion

import groovy.json.JsonSlurper
import org.ishimura.bussines.ILoginBussines
import org.ishimura.dao.UsuarioRepository
import io.vertx.core.AbstractVerticle
import javax.inject.Inject
import com.squareup.okhttp.ConnectionSpec
import com.squareup.okhttp.FormEncodingBuilder
import com.squareup.okhttp.OkHttpClient
import com.squareup.okhttp.Request
import com.squareup.okhttp.RequestBody
import com.squareup.okhttp.Response
import groovy.json.JsonOutput

/**
 * LoginVerticle, servicio para realizar el acceso a datos de la api 
 * de UBER
 * */

class LoginVerticle extends AbstractVerticle  {
	ILoginBussines loginBussines
	@Inject
	LoginVerticle(ILoginBussines loginBussines){
		this.loginBussines= loginBussines
	}

	/**
	 * Callback regresa el access  token de uber
	 */
	private void handlerCallback(){
		vertx.eventBus().consumer("callback", {message->
			def jsonSlurper = new JsonSlurper()
			def object = jsonSlurper.parseText(message.body())
			vertx.executeBlocking({h->
				OkHttpClient client =  new OkHttpClient()
				RequestBody formBody = new FormEncodingBuilder().
						add("grant_type", "authorization_code")
						.add("code",  object.code)
						.add("redirect_uri", "https://servicios-gasolina.herokuapp.com/callback")
						.add("client_id", "SpkYnj4ovedxOAmiX8RycRZSIrb0Aavu")
						.add("client_secret", "Uti36shcAczJKxjPWUE67TE6VB4W9ufkFxA5j1OR")
						.build();
				Request request = new Request.Builder().url("https://login.uber.com/oauth/v2/token")
						.post(formBody).build()
				Response response = client.newCall(request).execute()
				def data = response.body().string()
				h.complete(data);
			},false, {s->
				message.reply(s.result())
			})
		})
	}

	/**
	 * Obtengo los datos de el usuario de uber
	 * la cual funcionara para crear token de usuario
	 *
	 * */
	private void handlerProfile(){
		vertx.eventBus().consumer("getProfile", {message->
			def jsonSlurper = new JsonSlurper()
			def object = jsonSlurper.parseText(message.body())
			def accesstoken=object.access_token
			vertx.executeBlocking({h->
				OkHttpClient client =  new OkHttpClient()
				Request request = new Request.Builder().url("https://api.uber.com/v1.2/me")
						.addHeader("Authorization", "Bearer ${accesstoken}")
						.get().build()
				Response response = client.newCall(request).execute()
				if(response.isSuccessful()){
					def data = response.body().string()
					def sluper= new JsonSlurper()
					def objecta= sluper.parseText(data)
					loginBussines.login(objecta.email)
					h.complete(data);
				}
			},false, {s->
				message.reply( JsonOutput.toJson(["data": s.result(), "code": accesstoken]));
			})
		})
	}

	/**
	 * Metodo que arranca los eventbus para realizar la validacion
	 * */
	public void start(){
		handlerCallback()
		handlerProfile()
	}
}
