package org.ishimura.implementacion

import io.vertx.core.AbstractVerticle

import org.ishimura.interfaces.IDaemon
import org.ishimura.interfaces.IDaoCatalogo
import org.ishimura.interfaces.IDaoGasolinera
import org.ishimura.interfaces.IRepository
import org.ishimura.webspider.DaoCatalogosHTTP;
import org.ishimura.webspider.DaoGasolinera;
import org.ishimura.webspider.RedisRepository;
import javax.inject.Inject


class RepositoryDaemon extends AbstractVerticle {
	IDaoCatalogo dao
	IRepository repository
	IDaoGasolinera repositoryGas
	long idPeriodic
	
	@Inject
	RepositoryDaemon(IDaoCatalogo dao, IRepository repository,IDaoGasolinera repositoryGas ){
		this.dao= dao
		this.repository= repository
		this.repositoryGas= repositoryGas
		
	}
	
	public void start(){
		this.vertx.executeBlocking({s->
			update()
			updateGas()
		}, null)
		
		idPeriodic= vertx.setPeriodic(3600000, {r-> update()})
		vertx.setPeriodic(3600000, {r->updateGas() })
	}
	void updateGas(){
			repositoryGas.init();
	}
	void update() {
	
			repository.init({r->
				def salida= dao.getMarcas()
				salida.each{  ele->
					ele.SubModelos= []
					def subModelo= dao.getSubModelos(ele.value)
					subModelo.each{ sub ->
						ele.SubModelos.add(sub)
					}
				}
				salida.each{ ele ->
					repository.save(ele)
				}
			})
	
	}
	public void stop() {
		vertx.cancelTimer(idPeriodic)
	}
}
