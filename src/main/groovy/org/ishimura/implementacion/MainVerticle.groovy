package org.ishimura.implementacion
import io.vertx.core.AbstractVerticle
import io.vertx.core.DeploymentOptions
import io.vertx.core.json.JsonObject
import org.ishimura.di.ServiceBinder

class MainVerticle extends AbstractVerticle {
	public void start(){
		def lista=[]
		lista.add(WebHost.class)
		lista.add(HandleEventBusVerticle.class)
		lista.add(LoginVerticle.class)
		lista.add(RepositoryDaemon.class)
		lista.add(RegisterVerticle.class)
		
		JsonObject config = new JsonObject()
		.put("guice_binder", ServiceBinder.class.getName());
		
		lista.forEach({verticle->
			DeploymentOptions opts = new DeploymentOptions()
			opts.setConfig(config)
			opts.setWorker(true)
			vertx.deployVerticle("java-guice:" + verticle.getName(), opts,  {deployResponse->
				if (deployResponse.failed())
					println("Unable to deploy verticle + ${verticle.getClass().getSimpleName()},${deployResponse.cause()}");
				else
					println("${verticle.getName()} deployed");
			})
		})
		println("Start")
	}
}
