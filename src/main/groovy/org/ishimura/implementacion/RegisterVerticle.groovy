package org.ishimura.implementacion

import groovy.json.JsonSlurper
import groovy.json.JsonOutput
import io.vertx.core.AbstractVerticle
import javax.inject.Inject
import org.bson.types.ObjectId
import org.ishimura.bussines.IRegistroBussines

class RegisterVerticle extends AbstractVerticle {

	IRegistroBussines registro;

	@Inject
	RegisterVerticle(IRegistroBussines registro){
		this.registro= registro
	}

	@Override
	public void start(){
		vertx.eventBus().consumer("api.myvehicules.getAll", {message->
			vertx.executeBlocking({h->
				def jsonSlurper = new JsonSlurper()
				def object = jsonSlurper.parseText(message.body())
				def data= registro.getAllByUser(object.username)
				h.complete(data)
			}, false, {s->
				message.reply(JsonOutput.toJson(s.result()))
			})
		})
		vertx.eventBus().consumer("api.myvehicules.post", {message->
			vertx.executeBlocking({h->
				def jsonSlurper = new JsonSlurper()
				def object = jsonSlurper.parseText(message.body())
				registro.save(object.username,jsonSlurper.parseText(object.data) )
				h.complete(['ok': true])

			}, false, {s->
				message.reply(JsonOutput.toJson(s.result()))
			})
		})
		vertx.eventBus().consumer("api.myvehicules.delete", {message->
			vertx.executeBlocking({h->
				def jsonSlurper = new JsonSlurper()
				def object = jsonSlurper.parseText(message.body())
				def data= registro.delete(object.username, object.data)
				h.complete(data)
				
			}, false, {s->
				message.reply(JsonOutput.toJson(s.result()))
			})
		})
		vertx.eventBus().consumer("api.myvehicules.get", {message->
			vertx.executeBlocking({h->
				def jsonSlurper = new JsonSlurper()
				def object = jsonSlurper.parseText(message.body())
				def data= registro.get(object.username, object.data)
				h.complete(data)
			}, false, {s->
				message.reply(JsonOutput.toJson(s.result()))
			})
		})
		vertx.eventBus().consumer("api.myvehicules.route", {message->
			vertx.executeBlocking({h->
				def jsonSlurper = new JsonSlurper()
				def object = jsonSlurper.parseText(message.body())
				registro.saveRoute(object.data)
				h.complete("OK")
			}, false, {s->
				message.reply(JsonOutput.toJson(s.result()))
			})
		})
	}
}
