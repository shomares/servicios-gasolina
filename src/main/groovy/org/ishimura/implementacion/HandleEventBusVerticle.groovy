package org.ishimura.implementacion

import org.ishimura.interfaces.IDaoCatalogo;
import javax.inject.Inject
import org.ishimura.webspider.DaoCatalogosHTTP;
import org.ishimura.webspider.EnumStatic;

import com.fasterxml.jackson.databind.ObjectMapper

import groovy.json.JsonSlurper
import io.vertx.core.AbstractVerticle
import io.vertx.redis.RedisClient
import io.vertx.redis.RedisOptions

class HandleEventBusVerticle extends AbstractVerticle {
	IDaoCatalogo dao
	RedisOptions config
	RedisClient client

	@Inject
	HandleEventBusVerticle(IDaoCatalogo dao){
		this.dao=  dao
		this.config = new RedisOptions()
				.setAuth("p6aa1829928d2bdc41a3acb873924d125fb8bda9b4f9c4c8b4fa125beda8afb03")
				.setHost("ec2-34-235-90-46.compute-1.amazonaws.com")
				.setPort(13659)
		this.dao= dao
	}
	public void start(){
		this.client= RedisClient.create(vertx, config)
		vertx.eventBus().consumer("org.ishimura.implementacion.getAno", { message->
			def jsonSlurper = new JsonSlurper()
			def object = jsonSlurper.parseText(message.body())
			def anos= dao.getAnos(object.marca, object.submodelo);
			def  ow = new ObjectMapper().writer().withDefaultPrettyPrinter()
			def anosStr= ow.writeValueAsString(anos)
			this.client.hset("${EnumStatic.KEYANOS}-${object.marca}",object.submodelo,  anosStr, {s->
				message.reply(anosStr);
			})
		})
		vertx.eventBus().consumer("org.ishimura.implementacion.getVehiculo", { message->
			def jsonSlurper = new JsonSlurper()
			def object = jsonSlurper.parseText(message.body())
			def vehiculos= dao.getVehiculos(object.marca, object.submodelo,object.ano);
			def  ow = new ObjectMapper().writer().withDefaultPrettyPrinter()
			def Str= ow.writeValueAsString(vehiculos)
			this.client.hset("${EnumStatic.KEYVEHICULOS}-${object.marca}-${object.submodelo}",object.ano, Str, {s->
				message.reply(Str);
			})
		})
		
	}
}
