package org.ishimura.implementacion

import io.vertx.core.AbstractVerticle
import io.vertx.core.MultiMap
import io.vertx.core.buffer.Buffer
import io.vertx.core.http.HttpServerOptions
import io.vertx.core.json.JsonObject
import io.vertx.core.net.JksOptions
import io.vertx.ext.auth.User
import io.vertx.ext.web.Router
import io.vertx.ext.web.RoutingContext
import io.vertx.ext.web.client.HttpResponse
import io.vertx.ext.web.client.WebClient
import io.vertx.ext.web.client.WebClientOptions
import io.vertx.ext.web.handler.*
import io.vertx.ext.auth.jwt.JWTAuth
import io.vertx.ext.auth.oauth2.OAuth2FlowType
import javax.inject.Inject

import org.ishimura.entities.Registro
import org.ishimura.interfaces.IDaemon
import org.ishimura.interfaces.IDaoCatalogo
import org.ishimura.interfaces.IRepository
import org.ishimura.webspider.RedisRepository;

import groovy.json.JsonBuilder;
import groovy.json.JsonOutput
import groovy.json.JsonSlurper


class WebHost  extends AbstractVerticle {
	def server
	/**
	 * Interfaz para acceder a la base de datos en memoria (Redis) {@repository IRepository}
	 *
	 * */
	IRepository repository;

	/**
	 * 
	 * Router
	 */
	Router router;


	/**
	 *
	 * Auth 
	 */
	JWTAuth jwt;

	/**
	 * 
	 * Constructor
	 */
	@Inject
	public WebHost(IRepository repository) {
		this.repository =  repository
	}

	/**
	 * Inicializa el punto de entrada de la API REST, la cual para poder consumirla
	 * es necesario logearse a la aplicacion de UBER para logearse
	 *
	 * * */
	@Override
	public void start() {
		server= vertx.createHttpServer()
		router= Router.router(vertx);
		router.route("/api/*").handler(BodyHandler.create());

		configureCORS()
		configureAuth()
		configureCallback()
		initalizeApiVehiculos()
		initalizeApiSession()
		initalizeApiRegistro()
		server.requestHandler(router.&accept).listen(Integer.parseInt(System.getenv('PORT')))
		//server.requestHandler(router.&accept).listen(7094)

	}


	/**
	 *
	 * Se configura CORS
	 */

	private void configureCORS(){
		router.route().handler(io.vertx.ext.web.handler.CorsHandler.create("*")
				.allowedMethod(io.vertx.core.http.HttpMethod.GET)
				.allowedMethod(io.vertx.core.http.HttpMethod.POST)
				.allowedMethod(io.vertx.core.http.HttpMethod.DELETE)
				.allowedMethod(io.vertx.core.http.HttpMethod.OPTIONS)
				.allowedHeader("Access-Control-Allow-Method")
				.allowedHeader("Access-Control-Allow-Origin")
				.allowedHeader("Access-Control-Allow-Authorization")
				.allowedHeader("Authorization")
				.allowedHeader("Content-Type"));
	}

	/**
	 * Se configura Auth JWT
	 * 
	 * **/	
	private void configureAuth(){
		jwt = JWTAuth.create(vertx, new JsonObject()
				.put("keyStore", new JsonObject()
				.put("type", "jceks")
				.put("path", "keystore.jceks")
				.put("password", "secret")));

		router.route("/api/*").handler(JWTAuthHandler.create(jwt, "/callback"));
	}

	/**
	 * Se solicita un token oauth2 para consumir el api de UBER
	 * Ademas se autentica en la aplicacion con un token jwt obteniendo el nombre
	 * del usuario para authenticar
	 */
	private void configureCallback(){
		router.get("/callback").handler({routingContext->
			def code = JsonOutput.toJson(["code" : routingContext.request().getParam("code")])
			vertx.eventBus().send("callback",code , {h->
				vertx.eventBus().send("getProfile", h.result().body(), {s->
					//Una vez teniendo el profile del usuario
					//podemos crear un jwt para la api
					def data = s.result().body();
					def jsonSlurper = new JsonSlurper()
					def object = jsonSlurper.parseText(data)
					def profile= jsonSlurper.parseText(object.data)
					def token= jwt.generateToken(new JsonObject().put("sub", profile.email).put("ubertoken", object.code).put("user", object.data))
					routingContext.response().putHeader("location", "/redirect?access_token=${token}").setStatusCode(302).end()
				})
			})
		})
		router.get("/redirect").handler({routingContext->
			def access_token= routingContext.request().getParam("access_token")
			def obj= JsonOutput.toJson(['access_token': access_token])
			routingContext.response().putHeader("content-type", "text/html")
			routingContext.response().end("<body><script>window.opener.postMessage('${obj}', '*');</script></body>")
		})
	}

	/**
	 * Se fijan los handlers que van a manejar el el api de vehiculos
	 */
	private void initalizeApiVehiculos(){
		router.get("/api/gas/:xa/:ya").handler({routingContext->
			Double x, y
			x= Double.parseDouble(routingContext.request().getParam("xa"))
			y= Double.parseDouble(routingContext.request().getParam("ya"))
			repository.findGasolineras(x, y ,{r->
				routingContext.response()
						.putHeader("content-type", "application/json")
						.putHeader("Access-Control-Allow-Origin", "*")
						.putHeader("Access-Control-Allow-Methods","GET, POST, PUT, DELETE, OPTIONS")
						.putHeader("Access-Control-Allow-Credentials", "true")
						.end(r.result());
			})
		})
		router.get("/api/modelos/:id").handler({ routingContext->
			//Consult redis async
			def response= routingContext.response()
			def idMarca= routingContext.request().getParam("id")
			repository.getMarca(idMarca, {r->
				routingContext.response()
						.putHeader("content-type", "application/json")
						.putHeader("Access-Control-Allow-Origin", "*")
						.putHeader("Access-Control-Allow-Methods","GET, POST, PUT, DELETE, OPTIONS")
						.putHeader("Access-Control-Allow-Credentials", "true")
						.end(r.result());
			})
		})
		router.get("/api/marcas").handler({ routingContext->
			repository.getMarcas({r->
				routingContext.response()
						.putHeader("content-type", "application/json")
						.putHeader("Access-Control-Allow-Origin", "*")
						.putHeader("Access-Control-Allow-Methods","GET, POST, PUT, DELETE, OPTIONS")
						.putHeader("Access-Control-Allow-Credentials", "true")
						.end(r.result());
			})
		})
		router.get("/api/ano/:marca/:submodelo").handler({ routingContext->
			def marca= routingContext.request().getParam("marca")
			def subModelo= routingContext.request().getParam("submodelo")
			repository.getAnos(marca, subModelo,{r->
				routingContext.response()
						.putHeader("content-type", "application/json")
						.putHeader("Access-Control-Allow-Origin", "*")
						.putHeader("Access-Control-Allow-Methods","GET, POST, PUT, DELETE, OPTIONS")
						.putHeader("Access-Control-Allow-Credentials", "true")

						.end(r.result());
			})
		})
		router.get("/api/vehiculo/:marca/:submodelo/:ano").handler({ routingContext->
			def marca= routingContext.request().getParam("marca")
			def subModelo= routingContext.request().getParam("submodelo")
			def ano= routingContext.request().getParam("ano")
			repository.getVehiculo(marca, subModelo,ano,{r->
				routingContext.response()
						.putHeader("content-type", "application/json")
						.putHeader("Access-Control-Allow-Origin", "*")
						.putHeader("Access-Control-Allow-Methods","GET, POST, PUT, DELETE, OPTIONS")
						.putHeader("Access-Control-Allow-Credentials", "true")
						.end(r.result());
			})
		})
	}

	/**
	 * 
	 * */

	private void initalizeApiSession(){
		router.get("/api/me").handler({RoutingContext routingContext->
			User user= routingContext.user()
			if(user!=null){
				def claims = user.principal();
				routingContext.response().putHeader("content-type", "application/json")
						.putHeader("Access-Control-Allow-Origin", "*")
						.putHeader("Access-Control-Allow-Methods","GET, POST, PUT, DELETE, OPTIONS")
						.putHeader("Access-Control-Allow-Credentials", "true")
				routingContext.response().end(claims.getString("user"));
			}
		})
	}
	/**
	 * 
	 */

	private void initalizeApiRegistro(){
		router.get("/api/myvehicules").handler({RoutingContext routingContext->
			User user= routingContext.user()
			if(user!=null){
				def claims = user.principal()
				def username= claims.getString("sub")
				vertx.eventBus().send("api.myvehicules.getAll", JsonOutput.toJson(['username':username]), {h->
					routingContext.response().putHeader("content-type", "application/json")
							.putHeader("Access-Control-Allow-Origin", "*")
							.putHeader("Access-Control-Allow-Methods","GET, POST, PUT, DELETE, OPTIONS")
							.putHeader("Access-Control-Allow-Credentials", "true")
					routingContext.response().end(h.result().body());
				})
			}
		})
		router.post("/api/myvehicules").handler({RoutingContext routingContext->
			User user= routingContext.user()
			if(user!=null){
				def claims = user.principal()
				def username= claims.getString("sub")
				vertx.eventBus().send("api.myvehicules.post", JsonOutput.toJson(['username': username, 'data':
					routingContext.getBodyAsString()]), {h->
					routingContext.response().putHeader("content-type", "application/json")
							.putHeader("Access-Control-Allow-Origin", "*")
							.putHeader("Access-Control-Allow-Methods","GET, POST, OPTIONS")
							.putHeader("Access-Control-Allow-Credentials", "true")
					routingContext.response().end(JsonOutput.toJson(['ok': true]));
				} )
			}
		})
		router.delete("/api/myvehicules/:id").handler({RoutingContext routingContext->
			User user= routingContext.user()
			if(user!=null){
				String id = routingContext.request().getParam("id")
				def claims = user.principal()
				def username= claims.getString("sub")
				vertx.eventBus().send("api.myvehicules.delete", JsonOutput.toJson(['username': username, 'data': id]),
				{h->
					routingContext.response().putHeader("content-type", "application/json")
							.putHeader("Access-Control-Allow-Origin", "*")
							.putHeader("Access-Control-Allow-Methods","GET, POST, OPTIONS, DELETE")
							.putHeader("Access-Control-Allow-Credentials", "true")
					routingContext.response().end(h.result().body());
				} )
			}
		})
		router.get("/api/myvehicules/:id").handler({RoutingContext routingContext->
			User user= routingContext.user()
			if(user!=null){
				String id = routingContext.request().getParam("id")
				def claims = user.principal()
				def username= claims.getString("sub")
				vertx.eventBus().send("api.myvehicules.get", JsonOutput.toJson(['username': username, 'data': id]),
				{h->
					routingContext.response().putHeader("content-type", "application/json")
							.putHeader("Access-Control-Allow-Origin", "*")
							.putHeader("Access-Control-Allow-Methods","GET, POST, OPTIONS")
							.putHeader("Access-Control-Allow-Credentials", "true")
					routingContext.response().end(h.result().body());
				} )
			}
		})
	}
}
