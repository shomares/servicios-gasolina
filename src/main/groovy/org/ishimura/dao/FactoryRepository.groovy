package org.ishimura.dao

interface FactoryRepository {

	RegistroRepository createRegistroRepositroy()
	
	UsuarioRepository createUsuarioRepository()
	
}
