package org.ishimura.dao.impl

import com.mongodb.Mongo

import java.util.List;

import org.bson.types.ObjectId
import org.ishimura.dao.RegistroRepository;
import org.ishimura.entities.Registro;
import org.ishimura.entities.Usuario;
import org.ishimura.entities.Vehiculo;
import org.ishimura.entities.Estimado;
import org.mongodb.morphia.Datastore
import org.mongodb.morphia.Morphia

class RegistroMongoRepository extends MongoRepository implements RegistroRepository {
	RegistroMongoRepository(){
		super()
	}

	@Override
	public void saveRegistro(Registro registro) {
		// TODO Auto-generated method stub
		def query = datastore.createQuery(Vehiculo.class).filter("id", registro.vehiculo.id)
		Vehiculo vehiculo = null
		def lista = query.asList()
		if(lista.size()> 0)
			vehiculo = lista.get(0)
		
		if(vehiculo!=null)
			registro.vehiculo= vehiculo
		else
			datastore.save(registro.vehiculo)
		datastore.save(registro)
	}

	@Override
	public List<Registro> getRegistroByUser(String user) {
		// TODO Auto-generated method stub
		List<Registro> salida=null;
		def query = datastore.createQuery(Usuario.class).filter("correo", user)
		def lista= query.asList() as List<Usuario>
		Usuario usuario=null
		if(lista!=null && lista.size()>0){
			usuario = lista.get(0)
			query= datastore.createQuery(Registro.class).filter("usuario", usuario)
			salida= query.asList() as List<Registro>
			salida.each{
				it.objectId= it.id.toHexString()
				new ObjectId(it.objectId)
				it.consumoPromedio= this.calculateRendimiento(it.vehiculo)
			}
		}
		return salida;

	}

	public Double calculateRendimiento(Vehiculo vehiculo) {
		// TODO Auto-generated method stub
		def query = datastore.createQuery(Registro.class).filter("vehiculo", vehiculo)
		def lista= query.asList() as List<Registro>
		Double suma=0.0d;
		if(lista!=null)
		{
			lista.each {s->
				suma+= s.consumoPersonalizado?:0
			}
		}
		return suma / lista.size();
	}

	@Override
	public Registro get(String user,  id) {
		// TODO Auto-generated method stub
		Registro salida=null;
		def query = datastore.createQuery(Usuario.class).filter("correo", user)
		def lista= query.asList() as List<Usuario>
		Usuario usuario=null
		if(lista!=null && lista.size()>0){
			usuario = lista.get(0)
			println(id)
			println(id.replace('"', ''))
			
			salida=  datastore.get(Registro.class, new ObjectId(id.replace('"', '')))
			salida.objectId = salida.id.toHexString()
			
			new ObjectId(salida.objectId)
			
		}
		return salida;
	}

	@Override
	public void delete(String user, id) {
		// TODO Auto-generated method stub
		Registro reg= get(user, id);
		datastore.delete(reg)
		
	}
	
	@Override
	public void saveRoute(def route){
		Estimado estimado= new Estimado()
		
		def query = datastore.createQuery(Registro.class).filter("id", route.register.id);
		Registro registro = null
		def lista = query.asList()
		if(lista.size()> 0)
		{
			registro = lista.get(0)
			estimado.consumoEco= route.consumoEco
			estimado.consumoPersonalizado= route.consumoPersonalizado
			estimado.consumoPromedio= route.consumoPromedio
			estimado.distancia= route.distancia
			registro.estimados.add(estimado)
			
			datastore.updateFirst(query, registro, false)
		}
		
		

		
		estimado.fecha= new Date()
		estimado.kilometrosRecorridos= route.distancia;
		estimado.consumoEco= route.consumoEco;
		estimado.consumoPersonalizado= route.consumoPersonalizado;
		
		
	}






}
