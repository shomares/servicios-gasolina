package org.ishimura.dao.impl

import org.ishimura.dao.UsuarioRepository;
import org.ishimura.entities.Usuario;
class UsuarioMongoRepository  extends MongoRepository implements UsuarioRepository{


	UsuarioMongoRepository(){
		super()
	}

	@Override
	public void saveUser(Usuario user) {
		// TODO Auto-generated method stub
		datastore.save(user)

	}

	@Override
	public Usuario getUser(String user) {
		// TODO Auto-generated method stub
		def query= datastore.createQuery(Usuario.class).filter("correo", user)
		def lista = query.asList()
		if(lista.size()> 0)
			return lista.get(0)
		else
			return null
	}
}
