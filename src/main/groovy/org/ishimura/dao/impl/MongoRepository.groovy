package org.ishimura.dao.impl

import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia

import com.mongodb.Mongo;
import com.mongodb.MongoClient
import com.mongodb.MongoClientURI

abstract class MongoRepository implements AutoCloseable  {
	
	protected MongoClient mongo
	protected Datastore datastore
	
	public MongoRepository(){
		mongo= new MongoClient(new MongoClientURI("mongodb://heroku_vdmcs722:hju2r7tnli9qvvch5nkj3iiauc@ds155587.mlab.com:55587/heroku_vdmcs722"))
		datastore = new Morphia().createDatastore(mongo, "heroku_vdmcs722")
	}
	
	
	@Override
	public void close() throws Exception {
		// TODO Auto-generated method stub
		datastore=null
		mongo.close()

	}
}
