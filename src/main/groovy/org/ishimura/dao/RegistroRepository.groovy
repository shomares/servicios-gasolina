package org.ishimura.dao

import org.bson.types.ObjectId
import org.ishimura.entities.Registro
import org.ishimura.entities.Usuario
import org.ishimura.beans.Vehiculo
import org.ishimura.entities.Estimado


/**
 * 
 * @author root
 *
 */
interface RegistroRepository  extends AutoCloseable{

	/**
	 * 
	 * @param registro
	 */
	void saveRegistro(Registro registro)


	/**
	 * 
	 * @param user
	 * @return
	 */
	List<Registro> getRegistroByUser(String user)


	/**
	 * 
	 * @param user
	 * @param id
	 * @return
	 */
	Registro get(String user, id)

	
	/**
	 * 
	 * @param user
	 * @param id
	 */
	void delete(String user,  id)
	
	void saveRoute(def route)
	
}
